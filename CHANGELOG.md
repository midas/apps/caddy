Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[Unreleased]
============

To be fixed
-----------

Upcoming
--------
- better user management
- README completion
- custom theme

Under consideration
-------------------
- improving Caddyfile template
- fill Caddyfile from config.ini (at least for url)

[0.2.0] - 2022-01-15
====================

Fixed
-----
- Caddyfile template functional

Added
-----
- update Caddy and CaddyAuth to last version

[0.1.0] - 2022-01-14
====================

Added
-----
- README content
- docker files content
- service content
- Caddyfile template

[0.0.1] - 2022-01-14
====================

Added
-----
- description files (README & CHANGELOG)
- docker files (Dockerfile & docker-compose.yml)
- service template