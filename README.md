# Midas app: Caddy

A powerful [reverse proxy](https://en.wikipedia.org/wiki/Proxy_server) using the web server **[Caddy](https://caddyserver.com/)**.

This app can handle all the incoming http and https traffic, and redirect it to other services. With the use of [caddy auth-portal plugin](https://authp.github.io/) it can manage users and authentication.

This repository is part of the project **Midas** (for Minimalist Docker/Alpine Server). More infos in the [project wiki](https://gitlab.ensimag.fr/groups/midas/-/wikis/home).