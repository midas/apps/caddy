# Image to build app from
FROM caddy:2.5.1-builder AS builder

RUN xcaddy build \
    --with github.com/greenpau/caddy-security@v1.1.8

FROM caddy:2.5.1

ARG theme

RUN apk add nss

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
COPY themes/${theme} /home/theme
